@component('mail::message')
# Hola {{ $user->name }}

Has cambiado tu correo electronico. Porfavor verificala usando el siguiente boton: 

@component('mail::button', ['url' => {{ route('verify', $user->verification_token) }}])
Confirmar Cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
