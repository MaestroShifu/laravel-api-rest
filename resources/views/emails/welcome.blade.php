@component('mail::message')
# Hola {{ $user->name }}

Gracias por crear una cuenta. Porfavor verificala usando el siguiente boton: 

@component('mail::button', ['url' => {{ route('verify', $user->verification_token) }}])
Confirmar Cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent