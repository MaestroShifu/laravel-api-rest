<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BuyerScopes implements Scope{
    //modifica la consulta tipica del modelo y agrgar la nueva consulta
    //los scopes tambien pueden ser locales para modificar ciertas consultas
    public function apply(Builder $builder, Model $model){
        $builder->has('transactions');
    }
}