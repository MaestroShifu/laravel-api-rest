<?php

namespace App;

use App\Product;
use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use SoftDeletes;
    
    //son los que pueden ser asignados de manera masiva
    protected $fillable = [
        'name',
        'description'
    ];

    protected $hidden = ['pivot'];

    protected $dates = ['deleted_at'];

    //añadir el transformer en los modelos
    public $transformer = CategoryTransformer::class;

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
