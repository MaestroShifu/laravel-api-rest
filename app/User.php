<?php

namespace App;

use App\Transformers\UserTransformer;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR = 'false';

    protected $table = 'users';
    protected $dates = ['deleted_at'];

    //añadir el transformer en los modelos
    public $transformer = UserTransformer::class;

    protected $fillable = [
        'name', 
        'email', 
        'password',
        'verified',
        'verification_token',
        'admin'
    ];

    //mutador cambia cuando se va a guardar en la base de datos
    public function setNameAttribute($valor){
        $this->attributes['name'] = strtolower($valor);
    }

    //accesors son los parametros transformados para mostrar en pantalla
    public function getNameAttribute($valor){
        return ucwords($valor);
    }

    public function setEmailAttribute($valor){
        $this->attributes['email'] = strtolower($valor);
    }

    /**
     * Estos son los datos que van a ser ocultados como respuesta en la respuesta json
     */
    protected $hidden = [
        'password', 
        'remember_token',
        // 'verification_token'
    ];

    public function esVerificado(){
        return $this->verified == User::USUARIO_VERIFICADO;   
    }

    public function esAdministrador(){
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificacionToken(){
        return str_random(40);
    } 
}
