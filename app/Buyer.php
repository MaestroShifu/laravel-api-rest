<?php

namespace App;

use App\Transaction;
use App\Scopes\BuyerScopes;
use App\Transformers\BuyerTransformer;

class Buyer extends User
{
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new BuyerScopes);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }

    //añadir el transformer en los modelos
    public $transformer = BuyerTransformer::class;
}
