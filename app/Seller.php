<?php

namespace App;

use App\Product;
use App\Scopes\SellerScopes;
use App\Transformers\SellerTransformer;

class Seller extends User
{
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new SellerScopes);
    }

    //añadir el transformer en los modelos
    public $transformer = SellerTransformer::class;

    public function products(){
        return $this->hasMany(Product::class);
    }
}
