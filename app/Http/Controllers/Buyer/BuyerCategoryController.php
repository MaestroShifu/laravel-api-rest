<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerCategoryController extends ApiController
{
    public function __construct(){
        parent::__construct();

        $this->middleware('scope:read-general')->only('index');
        $this->middleware('can:view,'.Buyer::class)->only('index');
    }
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        //collapse une las listas en una sola
        $categories = $buyer->transactions()->with('product.categories')->get()->pluck('product.categories')->collapse()->unique('id')->values();

        return $this->showAll($categories);
    }
}
