<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Transaction;
use App\Http\Controllers\ApiController;

class TransactionCategoryController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $categories = Transaction::findOrFail($id)->product->categories;

        return $this->showAll($categories);
    }
}
