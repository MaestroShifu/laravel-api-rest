<?php

namespace App\Http\Middleware;

use Closure;

class SignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     /**
      * Los moddleware funcionan de la siguiente manera
      * Se pueden ejecutar de las siguientes maneras
      * 1) antes de la rspuestas
      * 2) Despues de la respuesta
      * Esto varia en donde se contruya el metodo next
      */
    public function handle($request, Closure $next, $header = 'X-Name')
    {
        $response = $next($request);

        $response->headers->set($header, config('app.name'));

        return $response;
    }
}
