<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponser;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use Illuminate\Session\TokenMismatchException;

class Handler extends ExceptionHandler
{
    use ApiResponser; 
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //error validaciones de campos
        if($exception instanceof ValidationException){
            $this->convertValidationExceptionToResponse($exception, $request);
        }

        //error no encuentra dato en los modelos
        if($exception instanceof ModelNotFoundException){
            $modelo = class_basename($exception->getModel());
            return $this->errorResponse('No existe ninguna instancia de ['.$modelo.'] con el id especificado', 404);
        }

        //error de autenticacion
        if($exception instanceof AuthenticationException){
            if($this->isFrontend($request)){
                return redirect()->guest('login');
            }

            return $this->unauthenticated($request, $exception);
        }

        //error de autorizacion
        if($exception instanceof AuthorizationException){
            return $this->errorResponse('No posee permisos para ejecutar esta accion', 403);
        }

        //cuando la URL no corresponde
        if($exception instanceof NotFoundHttpException){
            return $this->errorResponse('No se encontro la URL especificada', 404);
        }

        //la peticion HTTP no es el adecuado
        if($exception instanceof MethodNotAllowedHttpException){
            return $this->errorResponse('El metodo especificado en la peticion no es valido', 405);
        }

        //Peticiones genericas del http en caso de algun error 
        if($exception instanceof HttpException){
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        //Peticiones genericas del http en caso de algun error 
        if($exception instanceof QueryException){
            $codigo = $exception->errorInfo[1];

            if($codigo = 1451){
                return $this->errorResponse('No se puede eliminar de forma permanente el recurso porque esta relacionado con algun otro', 409);
            }
        }

        //Error token crsf 
        if($exception instanceof TokenMismatchException){
            return redirect()->back()->withInput($request->all());
        }

        //manejo para errores cuando esta en modo debug
        if(config('app.debug')){
            return parent::render($request, $exception);
        }

        return $this->errorResponse('Falla inesperada. Intente luego', 500);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();

        if($this->isFrontend($request)){
            return $request->ajax() ? response()->json($errors, 422) : redirect()->back()->withInput($request->input())->withErrors($errors);
        }

        return $this->errorResponse($errors, 422);
    } 

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $this->errorResponse('No autenticado.', 401);
    }

    private function isFrontend($request){
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
