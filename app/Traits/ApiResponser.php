<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

trait ApiResponser{
    private function successResponse($data, $code){
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code){
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showAll(Collection $collection, $code = 200){

        //si la collecion es vacia
        if($collection->isEmpty()){
            return $this->successResponse(['data' => $collection], $code);
        }

        //sirve para transformar la coleccion
        $transformer = $collection->first()->transformer;

        $collection = $this->filterData($collection, $transformer);
        $collection = $this->sortData($collection, $transformer);

        $collection = $this->paginate($collection);
        $collection = $this->transformData($collection, $transformer);
        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $instance, $code = 200){

        //sirve para transformar la coleccion
        $transformer = $instance->transformer;
        $instance = $this->transformData($instance, $transformer);

        return $this->successResponse($instance, $code);
    }

    protected function showMessage($message, $code = 200){
        return $this->successResponse(['data' => $message], $code);
    }

    //apirest.dev/api/users?page=[numero de pagina] solo una pagina de 15 por default
    //apirest.dev/api/users?per_page=[Cantidad de registros]&page=[numero de pagina] 
    protected function paginate(Collection $collection){
        $rules = [
            'per_page' => 'integer|min:2|max:50'
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();

        $perPage = 15;

        if(request()->has('per_page')){
            $perPage = (int) request()->per_page;
        }

        $result = $collection->slice(($page - 1) * $perPage, $perPage)->values();

        $paginated = new LengthAwarePaginator($result, $collection->count(), $perPage, $page, ['path' => LengthAwarePaginator::resolveCurrentPage()]);
    
        $paginated->appends(request()->all());

        return $paginated;
    }

    //filtros para los datos
    //apirest.dev/api/users?[campo]&[campo2]
    protected function filterData(Collection $collection, $transformer){

        foreach(request()->query() as $query => $value){
            $attribute = $transformer::originalAttribute($query);

            if(isset($attribute, $value)){
                $collection = $collection->where($attribute, $value);
            }
        }

        return $collection;
    }

    //sirve para organizar datos por algun atributo
    //apirest.dev/api/users?sort_by=[dato a agrupar]
    protected function sortData(Collection $collection, $transformer){
        if(request()->has('sort_by')){
            $attribute = $transformer::originalAttribute(request()->sort_by);

            $collection = $collection->sortBy->{$attribute};
        }

        return $collection;
    }

    protected function transformData($data, $transformer){
        $transformation = fractal($data, new $transformer);

        return $transformation->toArray();
    }

    //almacenado en cache paa evitar sobrecargar la base de datos
    protected function cacheResponse($data){
        $url = request()->url();
        $queryParams = request()->query();

        //ordena un array por las claves
        ksort($queryParams);

        //construye una query para peticiones http
        $queryStrings = http_build_query($queryParams);

        $fullUrl = "{$url} ? {$queryStrings}";

        //remember (key unica, tiempo en minutos, funcion que cacheara)
        return Cache::remember($fullUrl, 15/60, function() use ($data) {
            return $data;
        }); 
    }
}