<?php

namespace App\Providers;

use Carbon\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Buyer' => "App\Policies\BuyerPolicy",
        'App\Seller' => "App\Policies\SellerPolicy",
        'App\User' => "App\Policies\UserPolicy",
        'App\Transaction' => "App\Policies\TransactionPolicy",
        'App\Product' => "App\Policies\ProductPolicy"
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('admin-action', function($user){
            return $user->isAdmin();
        });

        //para las rutas del passport
        Passport::routes();
        //para que expire el token para consumir como cliente
        Passport::tokensExpireIn(Carbon::now()->addMinutes(30));
        //Para cuando se refresque y obtener uno nuevo  para consumir como cliente
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        //grant_type implicito usarlo solo cuando no se peude almacenar segura
        Passport::enableImplicitGrant();

        Passport::tokensCan([
            'purchase-product' => 'Crear transaciones para comprar productos detrminados',
            'manage-products' => 'Crear, ver, actualizar y eliminar productos',
            'manage-account' => 'Obtener la informacion de la cuenta, nombre, email, estado (sin contraseña), modificar datos como email, nombre y contraseña- no se puede eliminar cuenta',
            'read-general' => 'Obtener informacion general, categorias donde se comrpa y se vende, productos vendidos o comprados, transacciones, comras y ventas',
        ]);
    }
}
