<?php

namespace App\Providers;

use App\Product;
use App\User;
use App\Mail\UserCreated;
use App\Mail\UserMailChanged;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // //resuelve un problema con motores de base de datos antiguas como mariaDb
        // Schema::defaultStringLength(191);

        //cuando el usuario es creado
        User::created(function($user){
            retry(5, function() use ($user){
                Mail::to($user->email)->send(new UserCreated($user));
            }, 100);
        });

        //cuando el usuario es creado
        User::updated(function($user){
            //verifica si a sido modificado
            if($user->isDirty('email')){
                //retry intentos - funcion a ejecutar - tiempo a realizar otraves la accion
                retry(5, function() use ($user) {
                    Mail::to($user->email)->send(new UserMailChanged($user));
                }, 100);
            }
        });

        //es un evento cuando el producto es actualizado
        Product::updated(function($product){
            if($product->quantity == 0 && $product->estaDisponible()){
                $product->status = Product::PRODUCTO_NO_DISPONIBLE;

                $product->save();
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
